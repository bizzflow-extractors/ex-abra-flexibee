import sys
import logging

from bizztreat_base.config import Config

logging.basicConfig(level=logging.INFO, stream=sys.stdout)

from flexibee.extractor import FlexiBeeExtractor


def main():
    config = Config(force_schema=False)

    FlexiBeeExtractor(
        base_url=config["base_url"],
        username=config["username"],
        password=config["password"],
        jobs=config["jobs"],
        output_folder=config.output_folder,
        paginate_by=config.get("paginate_by"),

    ).extract()


if __name__ == '__main__':
    main()
