import asyncio
import logging
import urllib.parse
from aiohttp import ClientSession, BasicAuth, ClientResponseError

logger = logging.getLogger(__name__)


class FlexiBeeClient:

    def __init__(self, base_url, username, password, paginate_by=None):
        self.base_url = base_url
        self.auth = BasicAuth(username, password)
        self.paginate_by = paginate_by or 10000

    def build_url(self, company, evidence, page, filter=""):
        start = page * self.paginate_by
        if filter:
            filter = f"/({filter})"
        else:
            filter = ""
        path = f"c/{company}/{evidence}{filter}/?limit={self.paginate_by}&start={start}&add-row-count=true&detail=full"
        return urllib.parse.urljoin(self.base_url, path)

    async def __get_items_from_url(self, session, url):
        logger.info(f"Gathering data from {url}")
        headers = {
            'Accept': 'application/json'
        }
        async with session.get(url, auth=self.auth, headers=headers) as response:
            # Need to read response in all cases before calling raise_for_status, cause in case of bad status code
            # it close connection, so it is not possible to read response
            await response.read()
            try:
                response.raise_for_status()
            except ClientResponseError as e:
                logger.error(f"API {url} returned an error, {e}")
                logger.error(f"{await response.text()}")
                raise e
            data = await response.json()
            logger.info(f"Data gathered from {url}")
            return data

    async def get_items(self, company, evidence, filter=""):
        first_page_url = self.build_url(company, evidence, 0, filter)
        async with ClientSession() as session:
            first_page_data = await self.__get_items_from_url(session, first_page_url)
        total_count = int(first_page_data['winstrom']['@rowCount'])
        logger.info(f"Total count for company {company} and evidence {evidence} of row by API is {total_count}")

        first_page_items = first_page_data['winstrom'][evidence]
        for item in first_page_items:
            yield item

        async with ClientSession() as session:
            tasks = []
            page_count = total_count // self.paginate_by + 1

            for page in range(1, page_count):
                url = self.build_url(company, evidence, page, filter)
                tasks.append(self.__get_items_from_url(session, url))
            for result in asyncio.as_completed(tasks):
                data = await result
                items = data['winstrom'][evidence]
                for item in items:
                    yield item