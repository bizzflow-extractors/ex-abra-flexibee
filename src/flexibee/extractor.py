import asyncio
import csv
import logging
import os.path

from flexibee.client import FlexiBeeClient

logger = logging.getLogger(__name__)


class FlexiBeeExtractor:

    def __init__(self, base_url, username, password, jobs, output_folder, paginate_by=None):
        self.output_folder = output_folder
        self.api_client = FlexiBeeClient(
            base_url,
            username,
            password,
            paginate_by,
        )
        self.jobs = jobs

    async def process_job(self, job_name, job_settings):
        logger.info(f"Start extracting data of job {job_name}")
        items = self.api_client.get_items(
            job_settings["company"],
            job_settings["evidence"],
            job_settings.get("filter"),
        )
        row_count = 0
        file_name = job_settings.get("", f"{job_name}.csv")
        file_full_path = os.path.join(self.output_folder, file_name)
        with open(file_full_path, 'w') as csvfile:
            writer = None
            async for item in items:
                if writer is None:
                    fieldnames = job_settings.get("columns") or item.keys()
                    writer = csv.DictWriter(csvfile, fieldnames=fieldnames, dialect=csv.unix_dialect)
                    writer.writeheader()
                filtered_item = {key: item.get(key) for key in fieldnames}
                writer.writerow(filtered_item)
                row_count += 1
        logger.info(f"Finish extracting data of job {job_name}, row_count={row_count}")

    def extract(self):
        logger.info("Starting extracting data")
        jobs = []
        for job_name, job_settings in self.jobs.items():
            jobs.append(self.process_job(job_name, job_settings))
        loop = asyncio.get_event_loop()
        loop.run_until_complete(asyncio.gather(*jobs))
        logger.info(f"Extraction end")
