# BizzFlow ABRA FlexiBee Extractor

* **autor:** Ondřej Lanč (ondrej.lanc@bizztreat.com)
* **created:** 2020-10-21
* **API Documentation** [https://www.flexibee.eu/api/dokumentace/](https://www.flexibee.eu/api/dokumentace/)

## Description

Extractor for pulling all records from ABRA FlexiBee into .csv files

## Configuration

Create or modify [`config.json`](./config/config.json) file. In config files are following args

* base_url - base url of API
* username
* password
* paginate_by - optional (default 10000), if you set it will pull data without pagination (it can timeout) 
* jobs - object described bellow

### Jobs
Object containing all endpoints for pulling. Name of job is name of output file (withotu .csv extension).
For using filters see [https://www.flexibee.eu/api/dokumentace/ref/filters](https://www.flexibee.eu/api/dokumentace/). When you do not filter result keep it blank or remove it completely.
List of records (evidence) can be found here [https://demo.flexibee.eu/c/demo/evidence-list](https://demo.flexibee.eu/c/demo/evidence-list) or better in your FlexiBee app.
You also should define list of columns you want to extract - it's optional without it colums are defined by first item.

 ```

"jobs": {
    "job-1-name": {
      "company": "company_name",
      "evidence": "ucetni-denik",
      "filter": "stitky='code:VIP'"
      "columns": ["column1", "column2"]
    },
}
```



