FROM python:3.10-bullseye

WORKDIR /code
COPY requirements.txt /code
RUN pip install --no-cache-dir -U -r requirements.txt
ADD src/ /code

ENTRYPOINT ["python", "-u", "main.py"]

